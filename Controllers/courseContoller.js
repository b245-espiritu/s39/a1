

const Course = require("../Models/coursesSchema");

const User = require('../Models/usersSchema');
const auth = require('../auth.js')

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/
module.exports.addCourse = (request, response)=>{

    const userData = auth.decode(request.headers.authorization);

    console.log(userData);
    
    
    
    return User.findById(userData._id)
    .then(result => {
       if(result.isAdmin){
        let input = request.body;

        let newCourse = new Course({
            name:input.name,
            description: input.description,
            price: input.price
        })
    
        return newCourse.save()
        .then(course=> {
            console.log(course);
            response.send('couse added')
        })
        .catch(error=>{
            console.log(error);
            response.send('must be admin to add course')
        })
       }
       else{
        response.send('must be admin to add course')
       }


    })

    .catch(error=>{
        console.log(error);
        response.send('must be admin to add course')
    })
   

}


///
