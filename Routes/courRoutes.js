const express = require('express')

const router = express.Router();

const courseController = require('../Controllers/courseContoller.js')

const auth = require('../auth.js')


// Route for creating a course

router.post("/addCourse", auth.verify, courseController.addCourse)


module.exports = router;