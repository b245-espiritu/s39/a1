const express = require('express')
const auth = require('../auth.js')
const router = express.Router();

const userController = require('../Controllers/userController.js')


router.post('/register', userController.userRegistration)

router.post('/login', userController.userAuthentication)

router.get('/details', auth.verify, userController.getProfile)

module.exports = router;